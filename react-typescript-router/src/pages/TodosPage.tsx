import React, { useState, useEffect } from 'react';
import { TodoForm } from '../components/TodoForm';
import { TodoList } from '../components/TodoList';
import Todo from '../models/Todo';

declare let confirm: (srt: string) => boolean;

export const TodosPage: React.FC = () => {
    const [todos, setTodos] = useState<Todo[]>([]);

    useEffect(() => {
        const saved = JSON.parse(localStorage.getItem('todos') || '[]') as Todo[];
        setTodos(saved);
    }, []);

    useEffect(() => {
        localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos]);

    const addHandler = (title: string): void => {
        const newTodo = {
            title: title,
            id: Date.now(),
            completed: false,
        };
        setTodos((prev) => [newTodo, ...prev]);
    };

    const toggleHandler = (id: number): void => {
        setTodos((prev) =>
            prev.map((todo) => {
                if (todo.id === id) {
                    todo.completed = !todo.completed;
                }
                return todo;
            }),
        );
    };

    const removeHandler = (id: number): void => {
        const shouldRemove = confirm('Вы уверены, что хотите удалить дело?');
        if (shouldRemove) {
            setTodos((prev) => prev.filter((todo) => todo.id !== id));
        }
    };

    return (
        <React.Fragment>
            <TodoForm onAdd={addHandler} />
            <TodoList todos={todos} onToggle={toggleHandler} onRemove={removeHandler} />
        </React.Fragment>
    );
};
