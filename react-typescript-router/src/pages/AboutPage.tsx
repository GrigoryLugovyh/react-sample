import React from 'react';
import { useHistory } from 'react-router-dom';

export const AboutPage: React.FC = () => {
    const history = useHistory();
    return (
        <React.Fragment>
            <h1>Страница информации</h1>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima molestiae commodi itaque delectus ipsa
                laboriosam aliquid eos illum quidem distinctio?
            </p>
            <button className="btn" onClick={(): void => history.push('/')}>
                Обратно к списку дел
            </button>
        </React.Fragment>
    );
};
