import React from 'react';
import Todo from '../models/Todo';

interface TodoListProps {
    todos: Todo[];
    onToggle: (id: number) => void;
    onRemove: (id: number) => void;
}

export const TodoList: React.FC<TodoListProps> = ({ todos, onToggle, onRemove }) => {
    if (todos.length === 0) {
        return <p className="center">Пока что нет дел</p>;
    }

    const removeHandler = (event: React.MouseEvent, id: number): void => {
        event.preventDefault();
        onRemove(id);
    };

    return (
        <ul>
            {todos.map((todo) => {
                const inputStyleClasses = ['todo'];

                if (todo.completed) {
                    inputStyleClasses.push('completed');
                }

                return (
                    <li className={inputStyleClasses.join(' ')} key={todo.id}>
                        <label>
                            <input type="checkbox" checked={todo.completed} onChange={onToggle.bind(null, todo.id)} />
                            <span>{todo.title}</span>
                            <i
                                className="material-icons red-text"
                                onClick={(event): void => removeHandler(event, todo.id)}
                            >
                                delete
                            </i>
                        </label>
                    </li>
                );
            })}
        </ul>
    );
};
