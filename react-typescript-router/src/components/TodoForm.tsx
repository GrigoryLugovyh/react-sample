import React, { useState } from 'react';

interface TodoFormProps {
    onAdd: (title: string) => void;
}

export const TodoForm: React.FC<TodoFormProps> = (props) => {
    const [title, setTitle] = useState('');

    const changeHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setTitle(event.target.value);
    };

    const keyPressHandler = (event: React.KeyboardEvent<HTMLInputElement>): void => {
        if (event.key === 'Enter') {
            props.onAdd(title);
            setTitle('');
        }
    };

    return (
        <div className="input-field mt2">
            <input
                value={title}
                onChange={changeHandler}
                onKeyPress={keyPressHandler}
                type="text"
                id="title"
                placeholder="Введите название дела"
            />
            <label className="active" htmlFor="title">
                Введите название дела
            </label>
        </div>
    );
};
