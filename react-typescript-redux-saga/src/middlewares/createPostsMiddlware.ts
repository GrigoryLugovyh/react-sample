import { Middleware } from 'redux';
import { CREATE_POST, PostsActionTypes } from '../store/postsTypes';
import { showAlert } from '../store/appActions';
import { AppActionTypes } from '../store/appTypes';

const spams: string[] = ['fuck', 'spam'];

// как правильно типизировать dispatch?
export const createPostsMiddlware: Middleware = ({ dispatch }) => (next) => (
    action: AppActionTypes | PostsActionTypes,
) => {
    if (action.type === CREATE_POST) {
        const found = spams.filter((w) => action.payload.title.includes(w));
        if (found.length) {
            return dispatch(showAlert());
        }
    }
    return next(action);
};
