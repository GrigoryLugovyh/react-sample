import React from 'react';

export const Loader: React.FC = () => {
    return (
        <div className="spinner-grow spinner-grow-sm" role="status">
            <span className="sr-only">Loading...</span>
        </div>
    );
};
