import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createPosts } from '../store/postsActions';
import { showAlert } from '../store/appActions';

interface PostFormProps {
    desc: string;
}

export const PostForm: React.FC<PostFormProps> = ({ desc }) => {
    const [title, setTitle] = useState('');

    const dispatch = useDispatch();

    const submitHandler = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();

        if (!title.trim()) {
            dispatch(showAlert());
            return;
        }

        dispatch(
            createPosts({
                id: Date.now(),
                title: title,
            }),
        );

        setTitle('');
    };

    const changeInputHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setTitle(event.target.value);
    };

    return (
        <form action="submit" onSubmit={submitHandler}>
            <h5>{desc}</h5>
            <div className="form-group">
                <label htmlFor="title">Заголовок поста</label>
                <input
                    className="form-control"
                    type="text"
                    id="title"
                    name="title"
                    value={title}
                    onChange={changeInputHandler}
                />
            </div>
            <button className="btn btn-success" type="submit">
                Создать
            </button>
        </form>
    );
};

/*
interface OwnProps {
    desc: string;
}

interface MapStateToProps {}

interface MapDispatchProps {
    createPosts: (post: IPost) => PostsActionTypes;
    showAlertAction: () => void;
}

type PostFormProps = OwnProps & MapStateToProps & MapDispatchProps;

const PostForm: React.FC<PostFormProps> = ({ desc, createPosts, showAlertAction }) => {
    const [title, setTitle] = useState('');

    const submitHandler = (event: React.FormEvent<HTMLFormElement>): void => {
        event.preventDefault();

        if (!title.trim()) {
            showAlertAction();
            return;
        }

        createPosts({
            id: Date.now(),
            title: title,
        });

        setTitle('');
    };

    const changeInputHandler = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setTitle(event.target.value);
    };

    return (
        <form action="submit" onSubmit={submitHandler}>
            <h5>{desc}</h5>
            <div className="form-group">
                <label htmlFor="title">Заголовок поста</label>
                <input
                    className="form-control"
                    type="text"
                    id="title"
                    name="title"
                    value={title}
                    onChange={changeInputHandler}
                />
            </div>
            <button className="btn btn-success" type="submit">
                Создать
            </button>
        </form>
    );
};

const mapStateToProps = (): MapStateToProps => {
    return {};
};

export default connect<MapStateToProps, MapDispatchProps, OwnProps, StateType>(mapStateToProps, {
    createPosts,
    showAlertAction,
})(PostForm);
*/
