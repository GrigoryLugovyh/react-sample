import React from 'react';
import { Post } from './Post';
import { useSelector } from 'react-redux';
import { StateType } from '../store/rootReducer';

interface LocalPostsProps {
    title: string;
}

export const LocalPosts: React.FC<LocalPostsProps> = ({ title }) => {
    const posts = useSelector((state: StateType) => {
        return state.posts.localPosts;
    });

    if (!posts.length) {
        return <p className="text-center">Пока что нет постов</p>;
    }

    return (
        <React.Fragment>
            <h2>{title}</h2>
            {posts.map((post) => {
                return <Post post={post} key={post.id} />;
            })}
        </React.Fragment>
    );
};
