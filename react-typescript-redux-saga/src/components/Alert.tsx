import React from 'react';

export const Alert: React.FC = () => {
    return (
        <div className="alert alert-warning" role="alert">
            A simple warning alert—check it out!
        </div>
    );
};
