import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Post } from './Post';
import { StateType } from '../store/rootReducer';
import { requestPosts } from '../store/postsActions';
import { Loader } from './Loader';

interface FetchedPostsProps {
    title: string;
}

export const FetchedPosts: React.FC<FetchedPostsProps> = ({ title }) => {
    const dispatch = useDispatch();

    const posts = useSelector((state: StateType) => {
        return state.posts.fetchedPosts;
    });

    const loading = useSelector((state: StateType) => {
        return state.app.loading;
    });

    if (loading) {
        return <Loader />;
    }

    if (!posts.length) {
        const fetchPostsHandler = () => {
            dispatch(requestPosts());
        };

        return (
            <button className="btn btn-primary" onClick={fetchPostsHandler}>
                Загрузить
            </button>
        );
    }

    return (
        <React.Fragment>
            <h2>{title}</h2>
            {posts.map((post) => {
                return <Post post={post} key={post.id} />;
            })}
        </React.Fragment>
    );
};
