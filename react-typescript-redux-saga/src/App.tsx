import React from 'react';
import { PostForm } from './components/PostForm';
import { LocalPosts } from './components/LocalPosts';
import { FetchedPosts } from './components/FetchedPosts';
import { useSelector } from 'react-redux';
import { StateType } from './store/rootReducer';
import { Alert } from './components/Alert';

const App: React.FC = () => {
    const alerting = useSelector((state: StateType) => {
        return state.app.alert;
    });

    return (
        <div className="container pt-3">
            {alerting && <Alert />}
            <div className="row">
                <div className="col">
                    <PostForm desc={'Создание поста'} />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <LocalPosts title={'Локальные посты'} />
                </div>
                <div className="col">
                    <FetchedPosts title={'Серверные посты'} />
                </div>
            </div>
        </div>
    );
};

export default App;
