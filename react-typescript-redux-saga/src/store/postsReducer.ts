import { IPost } from '../models/Post';
import { PostsActionTypes, CREATE_POST, FETCH_POSTS } from './postsTypes';

export interface IPostsState {
    localPosts: IPost[];
    fetchedPosts: IPost[];
}

const initialState: IPostsState = {
    localPosts: [],
    fetchedPosts: [],
};

export const postsReducer = (state: IPostsState = initialState, action: PostsActionTypes): IPostsState => {
    switch (action.type) {
        case CREATE_POST:
            return {
                ...state,
                localPosts: [...state.localPosts, action.payload],
            };
        case FETCH_POSTS:
            return {
                ...state,
                fetchedPosts: action.payload,
            };
        default:
            return state;
    }
};
