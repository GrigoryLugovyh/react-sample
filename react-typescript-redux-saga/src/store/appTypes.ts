export const SHOW_LOADER = 'APP/SHOW_LOADER';
export const HIDE_LOADER = 'APP/HIDE_LOADER';
export const SHOW_ALERT = 'APP/SHOW_ALERT';
export const HIDE_ALERT = 'APP/HIDE_ALERT';

interface ShowLoaderAction {
    type: typeof SHOW_LOADER;
}

interface HideLoaderAction {
    type: typeof HIDE_LOADER;
}

interface ShowAlertAction {
    type: typeof SHOW_ALERT;
}

interface HideAlertAction {
    type: typeof HIDE_ALERT;
}

export type AppActionTypes = ShowLoaderAction | HideLoaderAction | ShowAlertAction | HideAlertAction;
