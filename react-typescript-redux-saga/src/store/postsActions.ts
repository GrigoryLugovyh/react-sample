import { IPost } from '../models/Post';
import { CREATE_POST, PostsActionTypes, REQUEST_POSTS, FETCH_POSTS } from './postsTypes';

export const createPosts = (post: IPost): PostsActionTypes => {
    return {
        type: CREATE_POST,
        payload: post,
    };
};

export const requestPosts = (): PostsActionTypes => {
    return {
        type: REQUEST_POSTS,
    };
};

export const fetchPosts = (posts: IPost[]): PostsActionTypes => {
    return {
        type: FETCH_POSTS,
        payload: posts,
    };
};
