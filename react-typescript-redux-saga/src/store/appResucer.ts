import { AppActionTypes, SHOW_LOADER, HIDE_LOADER, SHOW_ALERT, HIDE_ALERT } from './appTypes';

export interface IAppState {
    loading: boolean;
    alert: boolean;
}

const initialState: IAppState = {
    loading: false,
    alert: false,
};

export const appReducer = (state: IAppState = initialState, action: AppActionTypes): IAppState => {
    switch (action.type) {
        case SHOW_LOADER: {
            return { ...state, loading: true };
        }
        case HIDE_LOADER: {
            return { ...state, loading: false };
        }
        case SHOW_ALERT: {
            return { ...state, alert: true };
        }
        case HIDE_ALERT: {
            return { ...state, alert: false };
        }
        default:
            return state;
    }
};
