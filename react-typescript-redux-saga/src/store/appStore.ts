import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { rootReducer } from './rootReducer';
import { createPostsMiddlware } from '../middlewares/createPostsMiddlware';
import { postsWatcher } from './postsSaga';

const saga = createSagaMiddleware();

const appStore = createStore(
    rootReducer,
    compose(
        applyMiddleware(thunk, saga, createPostsMiddlware),
        // @ts-ignore
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    ),
);

saga.run(postsWatcher);

export type AppDispatch = typeof appStore.dispatch;

export default appStore;
