import { IPost } from '../models/Post';

export const CREATE_POST = 'POST/CREATE_POST';
export const FETCH_POSTS = 'POST/FETCH_POSTS';
export const REQUEST_POSTS = 'POST/REQUEST_POSTS';

interface CreatePostAction {
    type: typeof CREATE_POST;
    payload: IPost;
}

interface RequestPostsAction {
    type: typeof REQUEST_POSTS;
}

interface FetchPostsAction {
    type: typeof FETCH_POSTS;
    payload: IPost[];
}

export type PostsActionTypes = CreatePostAction | RequestPostsAction | FetchPostsAction;
