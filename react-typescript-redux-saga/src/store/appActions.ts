import { SHOW_LOADER, AppActionTypes, HIDE_LOADER, SHOW_ALERT, HIDE_ALERT } from './appTypes';
import { AppDispatch } from './appStore';

export const showLoader = (): AppActionTypes => {
    return {
        type: SHOW_LOADER,
    };
};

export const hideLoader = (): AppActionTypes => {
    return {
        type: HIDE_LOADER,
    };
};

export const showAlert = (): any => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: SHOW_ALERT,
        });

        setTimeout(() => {
            dispatch(hideAlert());
        }, 3000);
    };
};

export const hideAlert = (): AppActionTypes => {
    return {
        type: HIDE_ALERT,
    };
};
