import { takeEvery, put, call, delay } from 'redux-saga/effects';
import { REQUEST_POSTS } from './postsTypes';
import { showLoader, hideLoader } from './appActions';
import { IPost } from '../models/Post';
import { fetchPosts } from './postsActions';
import axios from 'axios';

export function* postsWatcher() {
    yield takeEvery(REQUEST_POSTS, postWorker);
}

function* postWorker() {
    yield put(showLoader());
    yield delay(500);
    const payload = yield call(requestPosts);
    yield put(fetchPosts(payload));
    yield put(hideLoader());
}

async function requestPosts(): Promise<IPost[]> {
    const response = await axios.get('http://jsonplaceholder.typicode.com/posts?_limit=5}');
    return response.data as IPost[];
}
