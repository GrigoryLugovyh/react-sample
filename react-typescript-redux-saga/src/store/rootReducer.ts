import { combineReducers } from 'redux';
import { postsReducer } from './postsReducer';
import { appReducer } from './appResucer';

export type StateType = ReturnType<typeof rootReducer>;

export const rootReducer = combineReducers({
    app: appReducer,
    posts: postsReducer,
});
